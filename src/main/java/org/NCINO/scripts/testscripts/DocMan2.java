package org.NCINO.scripts.testscripts;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;



public class DocMan2 {
	
		
	
	
		public static void main (String [] args) throws Exception{
			
		
		//public void docUpload() throws Exception{
				
			
			Reusable re= new Reusable();
			re.createChromeDriver();
			
			WebDriver driver = new ChromeDriver();			
			driver.get("https://test.salesforce.com/");		
			driver.manage().window().maximize();	
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);				
			WebDriverWait wait = new WebDriverWait(driver, (long) 25);				  	
			String encodedPassword=""; 
			
		re.setElement(driver, "xpath","//*[@id='username']","prosenjit.paul@flagstar.com.ncinodev");
		re.setElement(driver, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));	
		re.Click(driver, "//*[@id='Login']");	
		Thread.sleep(5000);
		
		// Click on Loans Tab
		re.Click(driver, "//*[@id='01r290000000Y48_Tab']/a");
		// Select a Loan
		re.Click(driver, "//*[@id='bodyCell']/div[3]/div[1]/div/div[2]/table/tbody/tr[3]/th/a");
		Thread.sleep(8000);
	
		
		//Switch to DocMan iframe
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,950)");
		driver.switchTo().frame("066290000005IHa");  
		// Click on Application on DocMan
		re.Click(driver, "//*[@id='accordion']/div/div[1]/div[1]/div/div[6]/div/img");
		Thread.sleep(3000);
		
		try {
			re.Click(driver, "//*[@id='upload_link1']");
			
        } catch (Exception e) {
        	
        } finally {
        	
        	re.Click(driver, "//*[@id='upload_link0']");
        }

		Thread.sleep(5000);
		 String filepath[]={"C:\\nCino\\ExcelTestDocument.xlsx"};
		   

for (int i=0;i<filepath.length;i++){
	 
	 
   upload_files(filepath[i]);    
	
				
    }
			
				}
			
	
			
			
			  public static void upload_files(String filepath) throws InterruptedException, AWTException {
           	   Robot robot = new Robot();
                  Thread.sleep(1000);

                  //enter file name
                  StringSelection file = new StringSelection(filepath);
                  Toolkit.getDefaultToolkit().getSystemClipboard().setContents(file, null);
                  robot.keyPress(KeyEvent.VK_CONTROL);
                  robot.keyPress(KeyEvent.VK_V);
                  robot.keyRelease(KeyEvent.VK_V);
                  robot.keyRelease(KeyEvent.VK_CONTROL);

                  // Press Enter 
                  robot.keyPress(KeyEvent.VK_ENTER);
                  robot.keyRelease(KeyEvent.VK_ENTER);
			  
              }
			  


public static String decodeStr(String encodedStr) {
    byte[] decoded = Base64.decodeBase64(encodedStr);
                    
    return new String(decoded);

	}
			}
