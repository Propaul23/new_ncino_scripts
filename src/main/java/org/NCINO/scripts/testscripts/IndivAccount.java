package org.NCINO.scripts.testscripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class IndivAccount {
	
	 WebDriver driver;

	public void newCustomer() throws Exception {
		
		//driver = null;
		this.driver=driver;
		
		driver.findElement(By.xpath("//*[@id='Account_Tab']/a")).click();
		Thread.sleep(2000);     
		driver.findElement(By.xpath("//*[@id='hotlist']/table/tbody/tr/td[2]/input")).click();
		//Continue buttton      
		driver.findElement(By.xpath("//*[@id='bottomButtonRow']/input[1]")).click();
		Thread.sleep(3000);     
		driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[1]/td[1]/input")).sendKeys("Bobby");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[1]/td[2]/input")).sendKeys("Parker");
		//street
		Thread.sleep(2000);
		//driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[3]/td[1]/input")).sendKeys("2575 Somerset Blvd");
		//Tax ID
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80']/div[2]/table/tbody/tr[2]/td[2]/input")).sendKeys("434445748");
		//City
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103']")).sendKeys("Troy");
		//State
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:1:j_id103']")).sendKeys("Michigan");
		//Zip
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:2:j_id103']")).sendKeys("48084");
		//Click Search
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:Search']")).click();
		Thread.sleep(3000);
		//Click Create New customer    //*[@id="j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id109:j_id110:j_id111:j_id113"]
		//System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id109:j_id110:j_id111:j_id113']"))).getText());  			 
		driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:addNew")).click();
		
		// Welcome Details Checkpoint
		//System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='pg:frm:pb:welcomeSection:comp:pgblk:pgblk1']/div[1]/h3"))).getText());
		
		Thread.sleep(4000);
		driver.findElement(By.xpath("//*[@id='pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2']/option[7]")).click();
		//Phone
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:1:section7:section11']")).sendKeys("9177785578");
		Thread.sleep(2000);
		//Tax ID
		//driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:PgSection121:section1:5:section2:section4']")).sendKeys("888118322");
		//Confirm Tax ID
		
		driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:PgSection121:section1:6:section2:section4']")).sendKeys("434445748");
		//Email
		//Mothers Maiden Name
			Thread.sleep(6000);
			driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:7:section7:section9']")).click();
			Thread.sleep(4000);
			driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:7:section7:section9']")).sendKeys("Sele");
		driver.findElement(By.xpath("//*[@id='pg:frm:pb:PgSection121:j_id347:section6:4:section7:section9']")).sendKeys("homeowner@test.com");
		
	}

}
