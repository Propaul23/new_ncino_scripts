package org.NCINO.scripts.testscripts;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;



public class DocMan_Upload {
	
				@Test
				public void login() throws Exception{
				
				//Reusable pe= new Reusable();
				Reusable re= new Reusable();
				re.createChromeDriver();
				
				WebDriver driver = new ChromeDriver();			
				driver.get("https://test.salesforce.com/");		
				driver.manage().window().maximize();	
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);				
				WebDriverWait wait = new WebDriverWait(driver, (long) 25);				  	
				String encodedPassword=""; 
				
			re.setElement(driver, "xpath","//*[@id='username']","prosenjit.paul@flagstar.com.ncinodev");
			re.setElement(driver, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));	
			re.Click(driver, "//*[@id='Login']");	
			Thread.sleep(5000);
			
			// Click on Loans Tab
			re.Click(driver, "//*[@id='01r290000000Y48_Tab']/a");
			// Select a Loan
			re.Click(driver, "//*[@id='bodyCell']/div[3]/div[1]/div/div[2]/table/tbody/tr[3]/th/a");
			Thread.sleep(8000);
			/*   Switch to DocMan iframe
			
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,550)");
			driver.switchTo().frame("066290000005IHa");  
			// Click on Application on DocMan
			re.Click(driver, "//*[@id='accordion']/div/div[1]/div[1]/div/div[6]/div/img");
			
			
			try {
				re.Click(driver, "//*[@id='upload_link1']");
				
	        } catch (Exception e) {
	        	
	        } finally {
	        	
	        	re.Click(driver, "//*[@id='upload_link0']");
	        }
	
			Thread.sleep(5000);
			 String filepath[]={"C:\\nCino\\ExcelTestDocument.xlsx"};
  		   

    for (int i=0;i<filepath.length;i++){
 	 
 	 
       upload_files(filepath[i]);                    */
        
			 
			 Thread.sleep(8000);
        // Scroll Up
       // jse.executeScript("window.scrollBy(950,0)");
        
        driver.switchTo().frame("066290000005IA8");
		Thread.sleep(6000);
		// Click on Loan Details
		driver.findElement(By.xpath("//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[2]/a/span")).click();		    							 			    
								     
		Thread.sleep(5000);
		// Provide relationship Manager name
		re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[8]/div/div[2]/span[2]/span[18]/div/input", "Thomas linden");
		// Client Accepted Terms
		re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[14]/div/div[2]/span[2]/span[6]/div/input", "08/29/2018");
		
		// Company Background
		re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[16]/div/div[2]/span[2]/span[19]/textarea", "This company is BIG Business comapny that brings in a lot of business and they should be approved for the loan");
		
		// Click on Pricing and Payment Structure tab 
		re.Click(driver, "//*[@id='tertiary-navigation']/nc-tertiary-navigation-item[2]/a");
		
		Alert alert = driver.switchTo().alert();
		alert.accept();
		
		Thread.sleep(4000);
		// Select a Pricing Basis
		re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[1]/div/div[2]/span[2]/span[16]/select/option[2]");
		
		// Select Index
		re.Click(driver,"//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[2]/div/div[2]/span[2]/span[16]/select/option[2]");
		
		// Spread%
		re.setElement(driver,"xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[3]/div/div[2]/span[2]/span[15]/div/input", "3.00");
		
		// Interest Rate
		re.setElement(driver,"xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[4]/div/div[2]/span[2]/span[15]/div/input", "5.75");
		
		// Rate Floor
		re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[5]/div/div[2]/span[2]/span[15]/div/input", "2.75");
		
		// Rate Ceiling
		re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[6]/div/div[2]/span[2]/span[15]/div/input", "8.75");
		
		// Payment Type
		re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[7]/div/div[2]/span[2]/span[16]/select/option[3]");
		
		// Payment Schedule
		re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[8]/div/div[2]/span[2]/span[16]/select/option[5]");
		
		// Interest Rate Adjustment Frequency
		re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[9]/div/div[2]/span[2]/span[16]/select/option[6]");
		
		// Amortized Term (Months)
		re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[10]/div/div[2]/span[2]/span[8]/input", "200");
		
		// Loan Term (Months)
		re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[11]/div/div[2]/span[2]/span[8]/input", "180");
		
		// Interest Only Months
		re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[12]/div/div[2]/span[2]/span[8]/input", "20");
		
		// First Payment Date
		re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[13]/div/div[2]/span[2]/span[6]/div/input", "7/27/2018");
		
		// SWAP?
		re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[14]/div/div[2]/span[2]/span[16]/select/option[3]");
		
		// Hover Principal payment and get TEXT
		System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[15]/div/div[1]/span/div/i"))).getText());
																                              
		// Principal Payment
		re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[15]/div/div[2]/span[2]/span[5]/input", "21500");
		
		// PrePayment Penalty Description
		re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[17]/div/div[2]/span[2]/span[2]/input", "Late Penalty Payment of 1 Million Dollarsss");
		
		// Click Save
		re.Click(driver, "//*[@id='j_id0:j_id2:j_id121:j_id122:j_id191']/span/div[2]/button");
		
		Thread.sleep(12000);
		
		//Switch to DocMan iframe
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,550)");
	
		driver.switchTo().frame("066290000005IHa");  
		// Click on Application on DocMan
		re.Click(driver, "//*[@id='accordion']/div/div[1]/div[1]/div/div[6]/div/img");
	
		try {
			re.Click(driver, "//*[@id='upload_link1']");
			
        } catch (Exception e) {
        	
        } finally {
        	
        	re.Click(driver, "//*[@id='upload_link0']");
        }

		Thread.sleep(5000);
		 String filepath[]={"C:\\nCino\\ExcelTestDocument.xlsx"};
		   

for (int i=0;i<filepath.length;i++){
	 
	 
   upload_files(filepath[i]);    
	
				
    }
			
				}
			
			
			public static String decodeStr(String encodedStr) {
			    byte[] decoded = Base64.decodeBase64(encodedStr);
			                    
			    return new String(decoded);
			
				}
			
			  public static void upload_files(String filepath) throws InterruptedException, AWTException {
           	   Robot robot = new Robot();
                  Thread.sleep(1000);

                  //enter file name
                  StringSelection file = new StringSelection(filepath);
                  Toolkit.getDefaultToolkit().getSystemClipboard().setContents(file, null);
                  robot.keyPress(KeyEvent.VK_CONTROL);
                  robot.keyPress(KeyEvent.VK_V);
                  robot.keyRelease(KeyEvent.VK_V);
                  robot.keyRelease(KeyEvent.VK_CONTROL);

                  // Press Enter 
                  robot.keyPress(KeyEvent.VK_ENTER);
                  robot.keyRelease(KeyEvent.VK_ENTER);
           	   
              }
			}
