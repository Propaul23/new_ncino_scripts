package org.NCINO.scripts.testscripts;

import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;



public class NcinoLogin2 {
	
					@Test
					public void login() throws Exception{
						
					
					//Reusable pe= new Reusable();
					Reusable re= new Reusable();
					re.createChromeDriver();
					
					WebDriver driver = new ChromeDriver();			
					driver.get("https://test.salesforce.com/");		
					driver.manage().window().maximize();	
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);				
					WebDriverWait wait = new WebDriverWait(driver, (long) 25);				  	
					String encodedPassword="Encoded PWD here"; 
					
				re.setElement(driver, "xpath","//*[@id='username']","prosenjit.paul@flagstar.com.ncinodev");
				re.setElement(driver, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));	
				re.Click(driver, "//*[@id='Login']");	
				Thread.sleep(5000);
				
				// Click Customers Tab
				re.Click(driver, "//*[@id='Account_Tab']/a");
				re.Click(driver, "//*[@id='bodyCell']/div[3]/div[1]/div/div[2]/table/tbody/tr[2]/th/a");
				// Click Details  //*[@id="efpViews_0012900000IZCnt_option1"]/span[2]
				re.Click(driver, "//*[@id='efpViews_0012900000IZCnt_option1']/span[2]");
				// Scroll Down and Click New Opportunity  	
								  
				re.Click(driver, "//*[@id='0012900000IZCnt_RelatedOpportunityList']/div[1]/div/div[1]/table/tbody/tr/td[2]/input");
				// New Opportunity in the hover link
				//re.Click(driver, "//*[@id='0012900000IZCnt_00N1600000F48Zl']/div[1]/div/div[1]/table/tbody/tr/td[2]/input");
				
				//Switch to child window			
				driver.get("https://cs19.salesforce.com/006/e?nooverride=1&retURL=0012900000IZCnt&accid=0012900000IZCnt&opp3=Prosenjit+Paul+-&CF00NG0000009bNUU=Prosenjit+Paul&opp6=Employee+Referral&CF00N1600000EtHBR=Prosenjit+Paul&CF00NV00000013mHX_lkid=0017A00000Masph&CF00NV00000013mHX=969-Rochester+Hills%2FAdams+Rd&opp9=5/30/2018&00NG0000009cKDC=5/3/2018&");
				          
				Thread.sleep(2000);
				
				// Product type
				re.Click(driver, "//*[@id='00N29000000vTzg']/option[2]");
				// Product 
				re.Click(driver, "//*[@id='00N29000000vTzh']/option[7]");
				// Stage
				re.Click(driver, "//*[@id='opp11']/option[2]");
				
				re.Click(driver, "//*[@id='00N29000000vTzg']/option[2]");
				
				re.Click(driver, "//*[@id='00N29000000vTzg']/option[2]");
				// Lending Unit
				re.Click(driver, "//*[@id='00N29000001BwlI']/option[2]");
				
				// Amount
				re.setElement(driver, "xpath", "//*[@id='opp7']", "5525520");
				re.setElement(driver, "xpath", "//*[@id='00N290000011LwW']", "Testing POM freamework for Commerical lending application");
				
				re.Click(driver, "//*[@id='topButtonRow']/input[1]");
				Thread.sleep(8000);
				// Click Convert to ncino button
				re.Click(driver, "//*[@id='topButtonRow']/input[6]");
				// Click Convert Radio button
				re.Click(driver, "//*[@id='page:j_id92:ObjectsToConvert:notconverted:0:j_id149']/input");
				Thread.sleep(5000);
				// Click "Continue with selected items"
				re.Click(driver, "//*[@id='page:j_id92:ObjectsToConvert:continuebutton']");
				// Click Convert   
				re.Click(driver, "//*[@id='page:j_id92:buttons:j_id246']");
				Thread.sleep(4000);
				System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("066290000005IBN"))).getText()); 	
				// Switch to iframe 
				driver.switchTo().frame("066290000005IBN");  
				
				
				// Click Loan Facilities     
				Thread.sleep(5000);	  	   
				driver.findElement(By.xpath("//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[2]/a/span")).click();
				Thread.sleep(6000);    		
				System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='j_id0:j_id1:j_id623']/div[2]/div/div/div/div/div/div[1]"))).getText());
				// Click on Loan Name               												  
				driver.findElement(By.xpath("//*[@id='ncFacilities']/tree-grid/div/div/table/tbody/tr/td[1]/a")).click();	
				Thread.sleep(6000);          //*[@id='ncFacilities']/tree-grid/div/div/table/tbody/tr/td[1]/a
				// Verify the Relationship iframe page		   
				driver.switchTo().frame("066290000005IA8");
				Thread.sleep(6000);
				// Click on Loan Details
				driver.findElement(By.xpath("//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[2]/a/span")).click();		    							 			    
				Thread.sleep(5000);
				
				
				
				
				
				
				/*/ Click Loan facilities
				re.Click(driver, "//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[2]/a/span");
				Thread.sleep(5000);
				System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='j_id0:j_id1:j_id623']/div[2]/div/div/div/div/div/div[1]"))).getText());			  
				//driver.switchTo().frame("066290000005IHg");
				// Click on Loan Name        													   
				re.Click(driver, "//*[@id='ncFacilities']/tree-grid/div/div/table/tbody/tr/td[1]/a");
				Thread.sleep(4000); 
				driver.switchTo().frame("066290000005IA8");
				// Click on Loan Details tab on right
				re.Click(driver, "//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[2]/a/span");            */
				Thread.sleep(5000);
				// Provide relationship Manager name
				re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[8]/div/div[2]/span[2]/span[18]/div/input", "Thomas linden");
				// Click on Pricing and Payment Structure tab 
				re.Click(driver, "//*[@id='tertiary-navigation']/nc-tertiary-navigation-item[2]/a");
				
				Alert alert = driver.switchTo().alert();
				alert.accept();
				
				Thread.sleep(4000);
				// Select a Pricing Basis
				re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[1]/div/div[2]/span[2]/span[16]/select/option[2]");
					}
				
				
				
				
				public static String decodeStr(String encodedStr) {
				    byte[] decoded = Base64.decodeBase64(encodedStr);
				                    
				    return new String(decoded);
				
					}
				}
