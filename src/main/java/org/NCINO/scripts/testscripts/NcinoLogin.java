package org.NCINO.scripts.testscripts;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class NcinoLogin {
	
				@Test
				public void login() throws Exception{
				
				//Reusable pe= new Reusable();
				Reusable re= new Reusable();
				re.createChromeDriver();
				
				WebDriver driver = new ChromeDriver();			
				driver.get("https://test.salesforce.com/");		
				driver.manage().window().maximize();	
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);				
				WebDriverWait wait = new WebDriverWait(driver, (long) 25);				  	
				String encodedPassword="Encoded PWD here"; 
				
			re.setElement(driver, "xpath","//*[@id='username']","prosenjit.paul@flagstar.com.ncinodev");
			//re.setElement(driver, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));	
			re.setElement(driver, "xpath", "//*[@id='password']", "Flagstar5");
			re.Click(driver, "//*[@id='Login']");	
			Thread.sleep(5000);
			Random rnd = new Random();
			int n = 1000 + rnd.nextInt(5000);
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			
			
			 re.CaptureScreenshot(driver, "C:\\Users\\ppaul\\testscripts\\Screenshots\\1.png");
			// Click Customers Tab  	     C:\Users\ppaul\testscripts\Screenshots
			re.Click(driver, "//*[@id='Account_Tab']/a");
			re.Click(driver, "//*[@id='bodyCell']/div[3]/div[1]/div/div[2]/table/tbody/tr[2]/th/a");
			// Click Details  //*[@id="efpViews_0012900000IZCnt_option1"]/span[2]
			re.Click(driver, "//*[@id='efpViews_0012900000IZCnt_option1']/span[2]");
			// Scroll Down and Click New Opportunity  	
							  
			re.Click(driver, "//*[@id='0012900000IZCnt_RelatedOpportunityList']/div[1]/div/div[1]/table/tbody/tr/td[2]/input");
			// New Opportunity in the hover link
			//re.Click(driver, "//*[@id='0012900000IZCnt_00N1600000F48Zl']/div[1]/div/div[1]/table/tbody/tr/td[2]/input");
			
			//Switch to child window			
			driver.get("https://cs19.salesforce.com/006/e?nooverride=1&retURL=0012900000IZCnt&accid=0012900000IZCnt&opp3=Prosenjit+Paul+-&CF00NG0000009bNUU=Prosenjit+Paul&opp6=Employee+Referral&CF00N1600000EtHBR=Prosenjit+Paul&CF00NV00000013mHX_lkid=0017A00000Masph&CF00NV00000013mHX=969-Rochester+Hills%2FAdams+Rd&opp9=5/30/2018&00NG0000009cKDC=5/3/2018&");
			          
			Thread.sleep(2000);
			
			// Opportunity Name
			re.setElement(driver, "xpath", "//*[@id='opp3']", "Pro's opportunity"+ n);
			// Product type
			re.Click(driver, "//*[@id='00N29000000vTzg']/option[2]");
			// Product 
			re.Click(driver, "//*[@id='00N29000000vTzh']/option[7]");
			// Stage
			re.Click(driver, "//*[@id='opp11']/option[2]");
			
			re.Click(driver, "//*[@id='00N29000000vTzg']/option[2]");
			
			re.Click(driver, "//*[@id='00N29000000vTzg']/option[2]");
			// Lending Unit
			re.Click(driver, "//*[@id='00N29000001BwlI']/option[2]");
			
			// Amount
			re.setElement(driver, "xpath", "//*[@id='opp7']", "5525520");
			re.setElement(driver, "xpath", "//*[@id='00N290000011LwW']", "Testing POM framework for Commerical lending application");
			
			re.Click(driver, "//*[@id='topButtonRow']/input[1]");
			Thread.sleep(8000);
			// Click Convert to ncino button
			re.Click(driver, "//*[@id='topButtonRow']/input[6]");
			// Click Convert Radio button
			re.Click(driver, "//*[@id='page:j_id92:ObjectsToConvert:notconverted:0:j_id149']/input");
			Thread.sleep(5000);
			// Click "Continue with selected items"
			re.Click(driver, "//*[@id='page:j_id92:ObjectsToConvert:continuebutton']");
			// Click Convert   
			re.Click(driver, "//*[@id='page:j_id92:buttons:j_id246']");
			
			Thread.sleep(4000);
			// Close Pop-Up
		/*	  driver.get("https://cs19.salesforce.com/ui/core/activity/ActivityReminderPage?at=1527022530668");
			driver.findElement(By.xpath("//*[@id='dismiss_all']")).click();
			driver.close();       */
			
			
			
			
			Thread.sleep(5000);
			
			
			Thread.sleep(4000);
			System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("066290000005IBN"))).getText()); 	
			// Switch to iframe 
			driver.switchTo().frame("066290000005IBN");  
			
			 re.CaptureScreenshot(driver, "C:\\Users\\ppaul\\testscripts\\Screenshots\\2.png");
			// Click Loan Facilities     
			Thread.sleep(5000);	  	   
			driver.findElement(By.xpath("//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[2]/a/span")).click();
			Thread.sleep(6000);  
			// Verify All Facilities text
			System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='j_id0:j_id1:j_id623']/div[2]/div/div/div/div/div/div[1]"))).getText());
			// Click on Loan Name               												
			driver.findElement(By.xpath("//*[@id='ncFacilities']/tree-grid/div/div/table/tbody/tr/td[1]/a")).click();	
			Thread.sleep(6000);          
			// Verify the Relationship i-frame page	
			// Switch to iframe
			driver.switchTo().frame("066290000005IA8");
			Thread.sleep(6000);
			// Click on Loan Details
			driver.findElement(By.xpath("//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[2]/a/span")).click();		    							 			    
									     
			Thread.sleep(5000);
			// Provide relationship Manager name
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[8]/div/div[2]/span[2]/span[18]/div/input", "Thomas linden");
			// Client Accepted Terms
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[14]/div/div[2]/span[2]/span[6]/div/input", "08/29/2018");
			
			// Current Situation & Financial Stability
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[15]/div/div[2]/span[2]/span[19]/textarea", "Current Financial Stability is Good for this business customer");
			
			// Company Background
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[16]/div/div[2]/span[2]/span[19]/textarea", "This company is BIG Business company that brings in a lot of $$$ and they should be approved for the loan, the risk is low for this CusTomEr");
			
			//Thread.sleep(2000);
			//jse.executeScript("window.scrollBy(0,550)");
			
			Thread.sleep(5000);
			// Customer Notification Due
			//re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[3]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[2]/div/div[2]/span[2]/span[6]/div/input", "08/20/2018");
											
			// HMDA Reportable Checkbox
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[18]/div/div[2]/span[2]/span[1]/input");
			
			// Click on Pricing and Payment Structure tab 
			re.Click(driver, "//*[@id='tertiary-navigation']/nc-tertiary-navigation-item[2]/a");
			
			Thread.sleep(3000);
			Alert alert = driver.switchTo().alert();
			alert.accept();
			
			Thread.sleep(4000);
			// Select a Pricing Basis
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[1]/div/div[2]/span[2]/span[16]/select/option[2]");
			
			// Select Index
			re.Click(driver,"//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[2]/div/div[2]/span[2]/span[16]/select/option[2]");
			
			// Spread%
			re.setElement(driver,"xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[3]/div/div[2]/span[2]/span[15]/div/input", "3.00");
			
			// Interest Rate
			re.setElement(driver,"xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[4]/div/div[2]/span[2]/span[15]/div/input", "5.75");
			
			// Rate Floor
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[5]/div/div[2]/span[2]/span[15]/div/input", "2.75");
			
			// Rate Ceiling
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[6]/div/div[2]/span[2]/span[15]/div/input", "8.75");
			
			// Payment Type
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[7]/div/div[2]/span[2]/span[16]/select/option[3]");
			
			// Payment Schedule
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[8]/div/div[2]/span[2]/span[16]/select/option[5]");
			
			// Interest Rate Adjustment Frequency
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[9]/div/div[2]/span[2]/span[16]/select/option[6]");
			
			// Amortized Term (Months)
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[10]/div/div[2]/span[2]/span[8]/input", "200");
			
			// Loan Term (Months)
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[11]/div/div[2]/span[2]/span[8]/input", "180");
			
			// Interest Only Months
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[12]/div/div[2]/span[2]/span[8]/input", "20");
			
			// First Payment Date
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[13]/div/div[2]/span[2]/span[6]/div/input", "7/27/2018");
			
			// SWAP?
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[14]/div/div[2]/span[2]/span[16]/select/option[3]");
			
			// Hover Principal payment and get TEXT
			System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[15]/div/div[1]/span/div/i"))).getText());
																	                              //*[@id="j_id0:j_id2:j_id624:j_id625:j_id638:j_id639"]/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[12]/div/div[1]/span/div/i
			// Principal Payment
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[15]/div/div[2]/span[2]/span[5]/input", "21500");
			
			// PrePayment Penalty Description
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[17]/div/div[2]/span[2]/span[2]/input", "Penalty Payment of $5000 Dollars$");
			
			// Click Save
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id121:j_id122:j_id191']/span/div[2]/button");
			
			Thread.sleep(6000);
			//alert.accept();
			
				// COMPLEX RATE STRUCTURES
				re.Click(driver, "//*[@id='tertiary-navigation']/nc-tertiary-navigation-item[3]/a");
				// Wait for Add Rate Stream Section
				System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='j_id0:j_id2:j_id624:j_id625:j_id626']/div[2]/div/div[1]/div[2]/div/div/div/div/div/div/div/div[2]/span/button"))).getText());
				Thread.sleep(4000);
				// Add Rate Stream
				re.Click(driver,    "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id626']/div[2]/div/div[1]/div[2]/div/div/div/div/div/div/div/div[2]/span/button");
				Thread.sleep(3000);
				// *Length of Stream 
				re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id626']/div[2]/div/div[2]/form/div[2]/div/div/div/div/div/div/div/div[1]/div[3]/div/div[1]/input", "100");
				Thread.sleep(3000);
				// *Index
				re.Click(driver, "//*[@id='index']/option[2]");
				Thread.sleep(3000);
				// Spread *
				Thread.sleep(3000);
				re.setElement(driver, "xpath", "//*[@id='spread']", "2.65");
				Thread.sleep(3000);
			// Save
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id626']/div[2]/div/div[2]/form/div[4]/div/div[2]/button");
			
					Thread.sleep(3000);
					// Add 2nd Rate Stream       
					Thread.sleep(3000);
					re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id626']/div[2]/div/div[1]/div[2]/div/div/div/div/div/div/div/div[2]/span/button");
					Thread.sleep(3000);	
					// *Length of Stream            
					Thread.sleep(3000);	
					re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id626']/div[2]/div/div[2]/form/div[2]/div/div/div/div/div/div/div/div[1]/div[3]/div/div[1]/input", "80");
					Thread.sleep(3000);	
					// *Index
					Thread.sleep(3000);	
					re.Click(driver, "//*[@id='index']/option[2]");
					Thread.sleep(3000);	
					// Spread *
					Thread.sleep(3000);	
					re.setElement(driver, "xpath", "//*[@id='spread']", "2.35");
					Thread.sleep(3000);	
			 // Save
			 re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id626']/div[2]/div/div[2]/form/div[4]/div/div[2]/button");
			 Thread.sleep(3000);
			
				
					
					// COMPLEX PAYMENT STRUCTURES			 	
					re.Click(driver, "//*[@id='tertiary-navigation']/nc-tertiary-navigation-item[4]/a"); 
					Thread.sleep(4000);																	  
					System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[2]/span/button"))).getText());
					Thread.sleep(3000);
					// Add Payment Stream
					re.Click(driver, "//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[2]/span/button");
					Thread.sleep(3000);    
					// Length of Stream
					re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[1]/div[2]/div/div[1]/input", "110");
					Thread.sleep(3000);
					// Payment Type (Principal & Interest) 
					new Select(driver.findElement(By.xpath("//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[2]/div[1]/select"))).selectByVisibleText("Principal & Interest");
					//new Select(re.Click(driver, "//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[2]/div[1]/select")).selectByVisibleText("Principal & Interest");
					
					// re.Click(driver, "//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[2]/div[1]/select/option[1]");					            
				Thread.sleep(3000);   
			// Save     
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[4]/div/div[2]/button/span");	
			Thread.sleep(3000); 
					System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[2]/span/button"))).getText());
					// Add Payment Stream                                                                
					re.Click(driver, "//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[2]/span/button");
				    Thread.sleep(3000);   
					// Length of Stream
				    re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[1]/div[2]/div/div[1]/input", "70");
				    Thread.sleep(3000);  
				    // Payment Type 
				    new Select(driver.findElement(By.xpath("//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[2]/div[1]/select"))).selectByVisibleText("Principal & Interest");
				    Thread.sleep(3000);  
				    //re.Click(driver, "//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[2]/div[1]/select/option[1]");
			 // Save
			 re.Click(driver, "//*[@id='j_id0:j_id2:j_id624']/div[2]/div/form/div/div[2]/div/div/div/div/div/div/div/div[4]/div/div[2]/button/span");	
		     Thread.sleep(7000);
		 		  
		     
				     
				     // DOC PREP DETAILS
				 	 re.Click(driver,"//*[@id='tertiary-navigation']/nc-tertiary-navigation-item[5]/a");
				 	 Thread.sleep(5000);
				 	 // Docs Reviewed by Line (Check-Box)
				 	 re.Click(driver,"//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[1]/div/div[2]/span[2]/span[1]/input");
				 	 Thread.sleep(3000);
				 	 // Pre-Closing Conditions Met (CheckBox)
				 	 re.Click(driver,"//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[2]/div/div[2]/span[2]/span[1]/input");
				 	Thread.sleep(3000);
				 	 // LaserPro Payment Type
				 	 re.Click(driver,"//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[4]/div/div[2]/span[2]/span[16]/select/option[3]");
					 Thread.sleep(2000);
				 	 // Attorney
					 re.setElement(driver,"xpath","//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[3]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[2]/div/div[2]/span[2]/span[2]/input","Johnnie Cochran");
				 	 // Click on Matter Number (i)
					 Thread.sleep(3000);
					 re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[3]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[3]/div/div[1]/span/div/i");
				 	 Thread.sleep(4000);
		 	 // Save
		 	 re.Click(driver,"//*[@id='j_id0:j_id2:j_id121:j_id122:j_id191']/span/div[2]/button");
		     
		 	 
		 	 	  Thread.sleep(5000);
			 	  // CLOSING DETAILS
			 	  re.Click(driver,"//*[@id='tertiary-navigation']/nc-tertiary-navigation-item[6]/a");	
			 	  // Clear to Close (Check-Box)
			 	  re.Click(driver,"//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[1]/div/div[2]/span[2]/span[1]/input");
			 	  // Post Closing Review Completed Date
			 	  re.setElement(driver, "xpath","//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[4]/div/div[2]/span[2]/span[6]/div/input","09/20/2018");
			 	  Thread.sleep(2000);
			 	  // Post Closing Review Comments
			 	 // re.setElement(driver,"xpath","/html/body/p","Script is still running.....");
		  // Save
		  re.Click(driver,"//*[@id='j_id0:j_id2:j_id121:j_id122:j_id191']/span/div[2]/button");
		  Thread.sleep(4000);
		 	 
		  						
			 // BALANCE DETAILS    //*[@id="tertiary-navigation"]/nc-tertiary-navigation-item[7]/a
			 	 re.Click(driver,"//*[@id='tertiary-navigation']/nc-tertiary-navigation-item[7]/a");
			 	 Thread.sleep(2000);
			 	 // Principal Balance
			 	 re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[2]/div/div[2]/span[2]/span[5]/input", "5500000");
			 	 // Total Disbursed
			 	 re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[3]/div/div[2]/span[2]/span[5]/input", "5525520"); 
			 	 // Total Undisbursed
			 	 re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[4]/div/div[2]/span[2]/span[5]/input", "525520");
			 	 // Net Exposure
			 	 re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[5]/div/div[2]/span[2]/span[5]/input", "2550500");
			 	 // % Retained
			 	 re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[6]/div/div[2]/span[2]/span[15]/div/input", "4.45874");
			 	 // Accrued Interest
			 	 re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[7]/div/div[2]/span[2]/span[5]/input", "357841");
			 	 // Total Charge Offs
			 	 re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[8]/div/div[2]/span[2]/span[5]/input", "125000");
			 	 // UNGTD Balance
			 	 re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[9]/div/div[2]/span[2]/span[5]/input", "1135000");
			 	 // re.hover("xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[10]/div/div[1]/span/div/i","");
			 	 // Balloon Payment
			 	 re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[10]/div/div[2]/span[2]/span[5]/input", "2225000");
		 // Save
		 re.Click(driver, "//*[@id='j_id0:j_id2:j_id121:j_id122:j_id191']/span/div[2]/button");
		 Thread.sleep(5000); 
		 	 
		
			 	// BOOKED DETAILS  //*[@id='tertiary-navigation-dropdown']/div/ul/li[5]/a
			 	re.Click(driver, "//*[@id='tertiary-navigation']/nc-tertiary-navigation-item[8]/a");
			 	Thread.sleep(3000);
			 	// Post-Booking Review Complete (Check-Box)
			 	re.Click(driver,"//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[1]/div/div[2]/span[2]/span[1]/input");
			 	// Initial Advance
			 	re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[3]/div/div[2]/span[2]/span[5]/input", "1658740");
			 	// Current Interest Rate
			 	re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[2]/div/div[2]/span[2]/span[15]/div/input", "4.562");
			 	// Funding at Close
			 	re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[4]/div/div[2]/span[2]/span[5]/input", "500550");
			 	// Total Fee Income
			 	re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[7]/div/div[2]/span[2]/span[5]/input", "95650");
			 	// Guarantee Fee Paid (Check-Box)
			 	re.Click(driver,"//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[8]/div/div[2]/span[2]/span[1]/input");
			 	// Sub-Standard Loan (Check-Box)
			 	re.Click(driver,"//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[3]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[1]/div/div[2]/span[2]/span[1]/input");
		 // Save
		 re.Click(driver, "//*[@id='j_id0:j_id2:j_id121:j_id122:j_id191']/span/div[2]/button");
		 	
		 	
		 	
			System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ncScreenSectionFormBuilderTopbar']/nc-topbar-screen-section/div/div/div/div/div[1]/nc-field-label-template/div/div/label"))).getText());
			// Add Loan Team            
			driver.findElement(By.xpath("//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[3]/a/span"));
			Thread.sleep(5000);
			//System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("066290000005IA8"))).getText()); 	
			//re.Click(driver, "//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[3]/a/span");
			//driver.switchTo().frame("066290000005IA8");
			// Verify the Team Members page
			//System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[1]/div/div/div/div/div/div[1]/span/span"))).getText());
			
			// Magic Wand
			re.Click(driver, "//*[@id='tools-actions']");
			
			Thread.sleep(4000);
			// Collateral
			re.Click(driver, "//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[4]/a/span");
			
			    Thread.sleep(8000);        
			 // Add Collateral
			    System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ncCollateral2']/collateral/div/collateral-hierarchy-schedule/article/div/tree-grid/div/div[1]/div/button"))).getText());
			    Thread.sleep(4000);
			    re.Click(driver, "//*[@id='ncCollateral2']/collateral/div/collateral-hierarchy-schedule/article/div/tree-grid/div/div[1]/div/button");
			Thread.sleep(4000);  
			// Click Cancel for Collateral
			re.Click(driver, "//*[@id='ncCollateral2']/collateral/div/collateral-search-modal/div/div[1]/div/div[3]/button[1]");
			Thread.sleep(3000);
			// Loan Team 
			System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[3]/a/span"))).getText());
			Thread.sleep(3000);
			re.Click(driver, "//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[3]/a/span");
			Thread.sleep(3000);
			// Add New   
			System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span[1]/div/button"))).getText());
			Thread.sleep(3000);										                                              
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span[1]/div/button");
			Thread.sleep(4000);				  
			// Enter Team Member  //*[@id="j_id0:j_id2:j_id624:j_id625:j_id638:j_id639"]/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span[1]/div/button
		/*	   re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span[3]/span/span/div[1]/div/div[2]/span[2]/span[18]/div/input", "Luigi Franciosi");
			Thread.sleep(2000); 
			System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='LLC_BI__LoanTeam__c_LLC_BI__User__c']/div/table/tbody[2]/tr/td[1]"))).getText());
			re.Click(driver, "//*[@id='LLC_BI__LoanTeam__c_LLC_BI__User__c']/div/table/tbody[2]/tr/td[1]");
			re.CaptureScreenshot(driver, "C:\\Users\\ppaul\\testscripts\\Screenshots\\3.png");
			// Role
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span[3]/span/span/div[2]/div/div[2]/span[2]/span[18]/div/input", "Branch Manager");
			Thread.sleep(2000);
			System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='LLC_BI__LoanTeam__c_LLC_BI__Assigned_Role__c']/div/table/tbody[2]/tr/td"))).getText());
			re.Click(driver, "//*[@id='LLC_BI__LoanTeam__c_LLC_BI__Assigned_Role__c']/div/table/tbody[2]/tr/td");
			Thread.sleep(2000);       */
			// Click Save
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span[4]/div/button[1]");
			Thread.sleep(6000);  
			
			
			// Fees
			re.Click(driver, "//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[6]/a/span");
			System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='tertiary-navigation']/nc-tertiary-navigation-item[2]/a"))).getText());
			re.Click(driver, "//*[@id='tertiary-navigation']/nc-tertiary-navigation-item[2]/a");
		   Thread.sleep(4000);//*[@id="tertiary-navigation"]/nc-tertiary-navigation-item[2]/a
			// Add New Fee    
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id679:j_id680']");
		   Thread.sleep(3000);//*[@id="j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id679:j_id684"]
			// Fees Drop-Down (Non-Standard Fees)
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id719:j_id720:j_id723']/select/option[2]");
			Thread.sleep(3000);
			Actions actions = new Actions(driver);
			
			// Fee Type 
			actions.moveToElement(driver.findElement(By.xpath("//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:0:j_id745:0:j_id747']/option[4]")));
			//re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:0:j_id745:0:j_id747']/option[4]");
			
			// Fee Description
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:0:j_id745:1:j_id752']", "This is an automated FEE");
			// Fee Amount           	    
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:0:j_id745:2:j_id752']", "999.00");
			// Paid By
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:0:j_id745:3:j_id752']/option[2]");
			// Payable To     
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:0:j_id745:4:j_id752']", "Flagstar Commercial Dept");
			// Collection Method
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:0:j_id745:5:j_id752']/option[3]");
			// Paid At Closing
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:0:j_id745:6:j_id752']/option[2]");
			
			// Click Add Row to add another FEE
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id679:bottom:j_id684']");
			Thread.sleep(3000);
			// Fee Type
			actions.moveToElement(driver.findElement(By.xpath("//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:1:j_id745:0:j_id747']/option[6]")));
			//re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:1:j_id745:0:j_id747']/option[6]");
			// Fee Description
			Thread.sleep(2000);
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:1:j_id745:1:j_id752']", "This is a 2nd automated FEE");
			Thread.sleep(2000);
			// Fee Amount 
			Thread.sleep(2000);
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:1:j_id745:2:j_id752']", "666.00");
			Thread.sleep(2000);
			// Paid By                     
			Thread.sleep(2000);
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:1:j_id745:3:j_id752']/option[2]");
			Thread.sleep(2000);
			// Payable To
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:1:j_id745:4:j_id752']", "Flagstar Commercial Dept");
			Thread.sleep(2000);
			// Collection Method
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:1:j_id745:5:j_id752']/option[3]");
			Thread.sleep(2000);
			// Paid At Closing
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id744:1:j_id745:6:j_id752']/option[3]");
			Thread.sleep(2000);
			// Save
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id641:j_id645:j_id646:theform:j_id652:j_id679:j_id685']");
			Thread.sleep(5000);
			
			// Borrowing Base
			re.Click(driver, "//*[@id='ncSecondaryNavigation']/nc-secondary-navigation/div/div/div[2]/nc-secondary-navigation-item[11]/a/span");
			Thread.sleep(4000);
			// Borrowing Base formula
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[1]/div/div[2]/span[2]/span[16]/select/option[3]");
			// Additional Comments
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[1]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[2]/div/div[2]/span[2]/span[19]/textarea", "Test Automation in Borrowing Base section");
			// Other Ineligibles Section (Retainage check box)
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[5]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[1]/div/div[2]/span[2]/span[1]/input");
			// Ineligible Inventory (Raw Materials) check-box
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[9]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[2]/div/div[2]/span[2]/span[1]/input");
			// If Other, please specify
			Thread.sleep(3000);
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[9]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[4]/div/div[2]/span[2]/span[19]/textarea", "Ineligible Automation section being filled out");
			Thread.sleep(3000);
			// Lockbox Required
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[11]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[1]/div/div[2]/span[2]/span[16]/select/option[3]");
			// Field Exam Required
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[11]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[2]/div/div[2]/span[2]/span[16]/select/option[3]");
			Thread.sleep(3000);
			//Field Exam Fee
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[11]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[3]/div/div[2]/span[2]/span[16]/select/option[3]");
			Thread.sleep(3000);
			// Field Exam Frequency
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[11]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[4]/div/div[2]/span[2]/span[16]/select/option[3]");
			Thread.sleep(3000);
			// Comments
			re.setElement(driver, "xpath", "//*[@id='j_id0:j_id2:j_id624:j_id625:j_id638:j_id639']/span[11]/div[2]/span/div[2]/div/div/div/div/div/div/span/span/div[5]/div/div[2]/span[2]/span[19]/textarea", "Additional Info field being automated");
			// Save
			re.Click(driver, "//*[@id='j_id0:j_id2:j_id121:j_id122:j_id191']/span/div[2]/button");
			
			Thread.sleep(8000);
			
		/*		Thread.sleep(5000);
			driver.switchTo().frame("066290000005IHa");
			// Click Application in DocMan
			re.Click(driver, "//*[@id='accordion']/div/div[2]/div[1]/div/div[6]/div/img");
			
			// Click Browse
			re.Click(driver, "//*[@id='upload_link1']");
			re.upload_files("C:\\nCino\\ExcelTestDocument.xlsx");           */
			
			
		
			//Switch to DocMan iframe
			
			//JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,950)");
			driver.switchTo().frame("066290000005IHa");  
			// Click on Application on DocMan
			re.Click(driver, "//*[@id='accordion']/div/div[1]/div[1]/div/div[6]/div/img");
			Thread.sleep(3000);
			
			try {
				re.Click(driver, "//*[@id='upload_link1']");
				
	        } catch (Exception e) {
	        	
	        } finally {
	        	
	        	re.Click(driver, "//*[@id='upload_link0']");
	        }

			Thread.sleep(5000);
			 String filepath[]={"C:\\nCino\\ExcelTestDocument.xlsx"};
			   

	for (int i=0;i<filepath.length;i++){
		 
		 
	   upload_files(filepath[i]);    
		
					
	    }
				
					}
				
		
				
				
				  public static void upload_files(String filepath) throws InterruptedException, AWTException {
	           	   Robot robot = new Robot();
	                  Thread.sleep(1000);

	                  //enter file name
	                  StringSelection file = new StringSelection(filepath);
	                  Toolkit.getDefaultToolkit().getSystemClipboard().setContents(file, null);
	                  robot.keyPress(KeyEvent.VK_CONTROL);
	                  robot.keyPress(KeyEvent.VK_V);
	                  robot.keyRelease(KeyEvent.VK_V);
	                  robot.keyRelease(KeyEvent.VK_CONTROL);

	                  // Press Enter 
	                  robot.keyPress(KeyEvent.VK_ENTER);
	                  robot.keyRelease(KeyEvent.VK_ENTER);
				  
	              }
				  
				
			
			public static String decodeStr(String encodedStr) {
			    byte[] decoded = Base64.decodeBase64(encodedStr);
			                    
			    return new String(decoded);
			
				}
			}
