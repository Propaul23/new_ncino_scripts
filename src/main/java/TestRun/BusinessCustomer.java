package TestRun;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.NCINO.scripts.testscripts.Reusable;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;




public class BusinessCustomer
{
	@Test
	public void Business () throws Exception{
	
	//public static void main(String[] args) throws Exception
		
				Reusable re= new Reusable();
				re.createChromeDriver();
				
				WebDriver driver = new ChromeDriver();			
				driver.get("https://test.salesforce.com/");		
				driver.manage().window().maximize();	
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);				
				WebDriverWait wait = new WebDriverWait(driver, (long) 25);				  	
				String encodedPassword="Encoded PWD here"; 
				
				re.setElement(driver, "xpath","//*[@id='username']","prosenjit.paul@flagstar.com.ncinodev");
			//re.setElement(driver, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));	
			re.setElement(driver, "xpath", "//*[@id='password']", "Flagstar5");
			re.Click(driver, "//*[@id='Login']");	
			Thread.sleep(5000);
			Random rnd = new Random();
			int n = 100000 + rnd.nextInt(900000);
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			Thread.sleep(3000);
			 driver.findElement(By.linkText("Customers")).click();
			    driver.findElement(By.name("new")).click();
			  
			    new Select(driver.findElement(By.id("p3"))).selectByVisibleText("Business Customer");
			    //driver.findElement(By.id("p3")).click();
			    // clicl Continue
			    driver.findElement(By.xpath("(//input[@name='save'])[2]")).click();
			    Thread.sleep(5000);
			    // First name
			    driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).click();
			   
			    driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).clear();
			
			    driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).sendKeys("Paul");
			  
			    driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id84:j_id86")).clear();
		
			    driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id84:j_id86")).sendKeys("Prosenjit");
			
			    driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id93:j_id95")).click();
	
			    driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id93:j_id95")).clear();
	
			    driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id93:j_id95")).sendKeys("668"+n);
			
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103")).click();
			 
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103")).click();
			  
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103")).clear();
			
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:0:j_id103")).sendKeys("Troy");
			   
			    driver.findElement(By.xpath("//div[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck']/div/center")).click();
			
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:2:j_id103")).click();
			  
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:2:j_id103")).clear();
		
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:2:j_id103")).sendKeys("48084");
		
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:1:j_id103")).click();
		
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:1:j_id103")).clear();
			
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id102:1:j_id103")).sendKeys("MI");
			   
			    driver.findElement(By.xpath("//div[@id='j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id109']/div/table/tbody/tr[2]/td")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:Search")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:addNew")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2")).click();
			
			    new Select(driver.findElement(By.id("pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2"))).selectByVisibleText("Other");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2")).click();
			
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:Name:NameText")).click();
			
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:Name:NameText")).clear();
		
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:Name:NameText")).sendKeys("Carters LLC");
			
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:0:section3:sectionleft")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:0:section3:sectionleft"))).selectByVisibleText("Limited Liability Company (LLC)");
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:1:section3:sectionleft"))).selectByVisibleText("Federal Employer Tax ID");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:0:section3:sectionleft")).click();
			  
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:3:section3:sectionleft")).click();
			    
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:3:section3:sectionleft")).clear();
			  
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:3:section3:sectionleft")).sendKeys("668"+n);
			    Thread.sleep(2000);
			    // Beneficial Ownership
			    new Select(driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:j_id330:j_id336"))).selectByVisibleText("At least 51% ownership shares are held by a publicly traded business entity");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:1:section3:section4")).click();
			
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:1:section3:section4")).clear();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:1:section3:section4")).sendKeys("3130050045");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:2:section3:sectionEmail")).click();
			  
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:2:section3:sectionEmail")).clear();
			    
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:2:section3:sectionEmail")).sendKeys("3135574874");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:3:section3:section4")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:3:section3:section4")).clear();
			   
			    driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionleft:section2:3:section3:section4")).sendKeys("Rygst@tres.com");
			    Thread.sleep(4000);
			    driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id354")).click();
			    Thread.sleep(2000);   	  
			    driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id354")).clear();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id354")).sendKeys("4400 corporate dr");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section16:j_id361")).click();
			
			    driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section16:j_id361")).clear();
			
			    driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section16:j_id361")).sendKeys("Troy");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section20:j_id414")).click();
			
			    driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section20:j_id414")).clear();
			   
			    driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section20:j_id414")).sendKeys("48098");
			    Thread.sleep(2000);
			    driver.findElement(By.xpath("//div[@id='pg:frm:pb:addresssection1:addresssection2']/div/table")).click();
			    Thread.sleep(2000);
			    // Click Continue
			    driver.findElement(By.name("pg:frm:pb:j_id607")).click();
			    Thread.sleep(2000);  
			    //driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:1:section3:sectionleft")).click();
			  
			   // new Select(driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:1:section3:sectionleft"))).selectByVisibleText("Federal Employer Tax ID");
			    Thread.sleep(2000);
			   // driver.findElement(By.id("pg:frm:pb:PgSection:PgSectionright:section2:1:section3:sectionleft")).click();
			    
			    
			    // Click Continue
			    //driver.findElement(By.name("pg:frm:pb:j_id625")).click();
			    Thread.sleep(2000);
			    //driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:item4:iddt11")).click();
			    Thread.sleep(2000);
			   // ID Document Type
			    new Select(driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:item4:iddt11"))).selectByVisibleText("Business Papers");
			    Thread.sleep(3000);                
			    //driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:item4:iddt11")).click();
			    Thread.sleep(3000);
			    driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:j_id82:bizcd")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:j_id82:bizcd")).clear();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionLeft:j_id82:bizcd")).sendKeys("01/14/2009");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:pgSection:pgSectionRight:j_id95:docv")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:0:theitem:j_id179")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:0:theitem:j_id179")).clear();
			    Thread.sleep(2000);
			    driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:0:theitem:j_id179")).sendKeys("BusyDay");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList3:0")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList1"))).selectByVisibleText("State");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList3:1")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:4:theitem:selectList3:1")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:5:theitem:selectList3:1")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList1"))).selectByVisibleText("No");
			    Thread.sleep(3000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList1")).click();
			    Thread.sleep(2000);	
			    driver.findElement(By.xpath("(//option[@value='United States'])[2]")).click();
			    Thread.sleep(2000);		     
			    //driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:8:theitem:multiSelect")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:8:theitem:multiSelect']/option[1]")).click();
			    //driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:9:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:9:theitem:selectList1"))).selectByVisibleText("2 - 5");
			    Thread.sleep(2000);         		
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:9:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList1"))).selectByVisibleText("$100,000 - $499,999");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList1"))).selectByVisibleText("No");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList1"))).selectByVisibleText("No");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:13:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:13:theitem:selectList1"))).selectByVisibleText("No");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:13:theitem:selectList1")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:14:theitem:selectList2")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:14:theitem:selectList2"))).selectByVisibleText("6-25");
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:14:theitem:selectList2")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:15:theitem:selectList4:1")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:15:j_id268:childpanel:0:j_id354")).click();
			    Thread.sleep(2000);  
			    new Select(driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:15:j_id268:childpanel:0:j_id354"))).selectByVisibleText("PayUSA");
			    Thread.sleep(2000);
			    driver.findElement(By.name("pg:frm:pb:j_id135:comp:pgblk:pgblk1:pgblktable1:15:j_id268:childpanel:0:j_id354")).click();
			    Thread.sleep(2000);
			    
			    
			    
			   
			    
			    
			    // Click Save
			    driver.findElement(By.name("pg:frm:pb:j_id1081")).click();
			  // // Click Save & New Application
			    driver.findElement(By.name("pg:frm:pb:j_id1086")).click();
			    Thread.sleep(2000);        
			    driver.findElement(By.xpath("(//input[@name='save'])[2]")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id163:checki")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id163:checki"))).selectByVisibleText("1");
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id163:checki")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id172:savingsi")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id172:savingsi"))).selectByVisibleText("1");
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id158:j_id172:savingsi")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:0:j_id196")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:0:j_id196"))).selectByVisibleText("Commercial Bank in the U.S.");
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:0:j_id196")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:0:j_id205")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:0:j_id205"))).selectByVisibleText("Not Applicable");
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:0:j_id205")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:1:j_id196")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:1:j_id196"))).selectByVisibleText("Corporation");
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id194:1:j_id196")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:1:j_id205")).click();
			    Thread.sleep(2000);
			    new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:1:j_id205"))).selectByVisibleText("Not Applicable");
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:j_id186:j_id201:1:j_id205")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.name("j_id0:Applications:mainBlock:j_id349:j_id351")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:Search")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id246:j_id248")).clear();
			    Thread.sleep(2000);
			    driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id246:j_id248")).sendKeys("Adding");
			    Thread.sleep(2000);
			    driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id249:j_id251")).clear();
			    Thread.sleep(2000);
			    driver.findElement(By.name("j_id0:Applications:mainBlock:j_id245:j_id249:j_id251")).sendKeys("Customer");
			    Thread.sleep(2000);
			    driver.findElement(By.id("j_id0:Applications:mainBlock:Search")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.name("j_id0:Applications:mainBlock:j_id315:j_id316:0:j_id318")).click();      
			    Thread.sleep(2000);
			    driver.findElement(By.name("j_id0:Applications:mainBlock:j_id349:j_id351")).click();
			    Thread.sleep(4000);		  //j_id0:Applications:mainBlock:j_id349:j_id351
			    
			    
			    // Ownership Checking Subtype  Select  Tax Reported Owner
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id166:equifaxpanel']/div/center/table/tbody/tr[4]/td[2]/select/option")).click();
			    Thread.sleep(2000);
			    // Ownership Savings account Ownership Subtype Select Tax Reported Owner
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id166:equifaxpanel']/div/center/table/tbody/tr[4]/td[3]/select/option")).click();
			    Thread.sleep(2000);
			    // Second Checking customer Select Authorized Signer
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id166:equifaxpanel']/div/center/table/tbody/tr[5]/td[2]/select/option[1]")).click();
			    Thread.sleep(2000);
			    // Second Savings customer Select Authorized Signer
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id166:equifaxpanel']/div/center/table/tbody/tr[5]/td[3]/select/option[1]")).click();
			    Thread.sleep(2000);
			    // Click Validate and Submit to Equifax button
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:ValidateSubmitEquifax']")).click();
			    Thread.sleep(7000);
			    //Provide NAICS Code
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id267']")).sendKeys("238150");
			    Thread.sleep(2000);
			    //Provide NAICS Description
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id269']")).sendKeys("238150");
			    Thread.sleep(2000);
			    // Select Pass
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningInput']/option[2]")).click();
			    Thread.sleep(3000);
			    // Select Pass
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningInput']/option[2]")).click();
			    Thread.sleep(2000);
			    // Select Pass for Second customer
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:2:warningInput']/option[2]")).click();
			    Thread.sleep(2000);
			    // Select Pass for Second customer
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:3:warningInput']/option[2]")).click();
			    Thread.sleep(2000);
			    // Override Notes/Comments First Customer
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:0:warningOverrideInput']")).sendKeys("Overriding for 1st customer");
			    Thread.sleep(2000);
			    // 2nd Override Notes/Comments First Customer
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:1:warningOverrideInput']")).sendKeys("2nd Override");
			    Thread.sleep(2000);
			    // Override for 2nd Cuistomer
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:2:warningOverrideInput']")).sendKeys("gjgjg");
			    Thread.sleep(2000);
			    // 2nd Override for 2nd Cuistomer
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id271:decisionTable:3:warningOverrideInput']")).sendKeys("hidguewdf");
			    Thread.sleep(2000);
			    //Click the Next button
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:equifaxTable:j_id446']/input")).click();
			    Thread.sleep(7000);
			    // Select Standard Business Checking from Dorp-down
			    driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:Product:j_id94")).click();
			    new Select(driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:Product:j_id94"))).selectByVisibleText("Standard Business Checking");
			    Thread.sleep(2000);
			    //Select Yes for Reg E Opt In
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:Checking:j_id133:j_id136']/div/select/option[2]")).click();
			    Thread.sleep(2000);
			    // Is there a beneficial owner with 25% or more company ownership who is not a signer on this account?  NO
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList3:1']")).click();
			    Thread.sleep(2000);          
			    // What is the purpose of the account?  General operations
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1']/option[3]")).click();
			    
			   // new Select(driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:j_id133:j_id138"))).selectByVisibleText("Yes");
			    Thread.sleep(2000);
			    // How many cash deposits or withdrawals do you expect to make per month?  0
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList3:0']")).click();
			    Thread.sleep(2000);
			    // Will deposits into this account represent cash, credit card, and/or check sales?  Yes Credit Card
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList1']/option[3]")).click();
			    Thread.sleep(2000);
			    // Do you expect seasonal fluctuations in account activity?  Yes- Winter
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:4:theitem:selectList3:0']")).click();
			    Thread.sleep(2000);
			    // Will any funds be accepted from countries other than the United States?  No
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:5:theitem:selectList3:1']")).click();
			    Thread.sleep(2000);
			    // How many domestic electronic withdrawals or deposits do you expect to conduct per month, if any?  0
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList3:0']")).click();
			    Thread.sleep(2000);
			    // How many electronic withdrawals or deposits from international companies/persons do you expect to conduct per month, if any?    0
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:7:theitem:selectList3:0']")).click();
			    Thread.sleep(2000);
			    // How many domestic wire transfers do you anticipate sending or receiving per month, if any?  0
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:8:theitem:selectList3:0']")).click();
			    Thread.sleep(2000);
			    // How many international wire transfers will you send or receive per month, if any?  0
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:9:theitem:selectList3:0']")).click();
			    Thread.sleep(2000);
			    // Does the business extend terms to customers?   No but Interested
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList1']/option[3]")).click();
			    Thread.sleep(2000);
			    // Do you intend to use any type of Remote Deposit Capture (RDC) services for check deposits?  No
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList3:1']")).click();
			    Thread.sleep(2000);
			    // What channel did the customer use to open the account?   In person - In Branch/ Office
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList1']/option[2]")).click();
			    Thread.sleep(2000);
			    // What is the source of initial funding for this account?  Business Proceeds
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:13:theitem:multiSelect']/option[1]")).click();
			    Thread.sleep(2000);          
			    // What is the source of ongoing funding for this account?   Withdrawal from another bank
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:14:theitem:multiSelect']/option[4]")).click();
			    Thread.sleep(2000);
			    // Will the business be using this account to fund the currency required for your ATM(s)?  No
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:0:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:15:theitem:selectList3:1']")).click();
			    Thread.sleep(2000);
			    
			    // Select Saving Account Product 
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:saving:svProd:j_id252']/div/select/option[2]")).click();
			    Thread.sleep(2000);
			    //What is the purpose of this account?  PayRoll
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList1']/option[2]")).click();
			    Thread.sleep(2000);
			    // Is there a beneficial owner with 25% or more company ownership who is not a signer on this account?
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList3:1']")).click();
			    Thread.sleep(2000);
			    // How many cash deposits or withdrawals do you expect to make per month?   0
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:2:theitem:selectList3:0']")).click();
			    Thread.sleep(3000);
			    // Will deposits into this account represent cash, credit card, and/or check sales?  Yes- Cash
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:3:theitem:selectList1']/option[2]")).click();
			    Thread.sleep(3000);
			    // Do you own and operate an Automated Teller Machine (ATM)?  No
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:4:theitem:selectList3:1']")).click();
			    Thread.sleep(2000);
			    // Do you expect seasonal fluctuations in account activity?  Yes- Spring
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:5:theitem:selectList3:1']")).click();
			    Thread.sleep(2000);
			    // Will you be conducting any international business?  No
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:6:theitem:selectList3:1']")).click();
			    Thread.sleep(2000);
			    // How many domestic electronic withdrawals or deposits do you expect to conduct per month, if any?  0
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:7:theitem:selectList3:0']")).click();
			    Thread.sleep(2000);
			    // How many electronic withdrawals or deposits from international companies/persons do you expect to conduct per month, if any?  0
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:8:theitem:selectList3:0']")).click();
			    Thread.sleep(2000);
			    // How many domestic wire transfers do you anticipate sending or receiving per month, if any?   0
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:9:theitem:selectList3:0']")).click();
			    Thread.sleep(2000);
			    // How many international wire transfers will you send or receive per month, if any?    0
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:10:theitem:selectList3:0']")).click();
			    Thread.sleep(3000);
			    // Does the business extend terms to customers?   No would not do so
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList1']/option[3]")).click();
			    //driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:11:theitem:selectList1']/option[4]")).click();
			    Thread.sleep(3000);
			    // Do you intend to use any type of Remote Deposit Capture (RDC) services for check deposits?   No
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:12:theitem:selectList3:1']")).click();
			    Thread.sleep(2000);
			    // What channel did the customer use to open the account?   In person - In Branch/ Office
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:13:theitem:selectList1']/option[2]")).click();
			    Thread.sleep(3000);
			    // What is the source of initial funding for this account?  ** INVESTMENT  **
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:14:theitem:multiSelect']/option[3]")).click();
			    Thread.sleep(2000);
			    // What is the source of ongoing funding for this account?  SALARY
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:15:theitem:multiSelect']/option[1]")).click();
			    Thread.sleep(2000);
			    // Will the business be using this account to fund the currency required for your ATM(s)?   NO
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:b2cList:1:kycSection:KYC:comp:pgblk:pgblk1:pgblktable1:16:theitem:selectList3:1']")).click();
			    Thread.sleep(2000);
			    // Click NEXT button
			    driver.findElement(By.xpath("//*[@id='pg:frm:pb:j_id828']/input[3]")).click();
			    Thread.sleep(6000);
			    // Click Next button  (have the adding Debit/ATM card option at this page
			    driver.findElement(By.xpath("//*[@id='addser:frm']/center/input[3]")).click();
			    Thread.sleep(5000);
			    // Funding Source   (Check)
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id98:0:j_id100:fundingcategorysection:fundingCategoryText']/option[3]")).click();
			    Thread.sleep(2000);
			    // Amount
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id98:0:j_id100:j_id131:j_id133']")).sendKeys("2500");
			    Thread.sleep(2000);
			    // Savings Funding Source  (Cash And Check)
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id98:1:j_id100:fundingcategorysection:fundingCategoryText']/option[4]")).click();
			    Thread.sleep(2000);
			    // Amount 
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id98:1:j_id100:j_id131:j_id133']")).sendKeys("5500");
			    Thread.sleep(2000);
			    // Click Next
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id83:j_id144:bottom']/input[3]")).click();
			    Thread.sleep(6000);
			    
			    // Setup Accounts   
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:j_id148:j_id206:setupUpAccountsbutton']")).click();
			    Thread.sleep(6000);
			    
			    driver.switchTo().alert().accept();
			    Thread.sleep(10000);
			    //Manually click OK on the pop-up button on top
			    // Not interested           
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:0:j_id107:0:pbs1:j_id108:0:j_id126:2']")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:0:j_id126:2']")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:1:j_id126:2']")).click();
			    Thread.sleep(2000);
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id106:1:j_id107:0:pbs1:j_id108:2:j_id126:1']")).click();
			    Thread.sleep(2000);
			    //Put a Comment 
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:j_id103:0:pgblcksection:j_id201:j_id202:textAreaDelegate_CrossSell_Notes__c_rta_body']")).sendKeys("This AUTOMATION test is almost done");
			    Thread.sleep(3000);
			    // Click Finish button
			    driver.findElement(By.xpath("//*[@id='j_id0:frm:pgblck:a:fin']")).click();
			    Thread.sleep(8000);
			    
	
	}


	public static String decodeStr(String encodedStr) 
	{
	               byte[] decoded = Base64.decodeBase64(encodedStr);
	                               
	               return new String(decoded);
	}
	
}