package katalon.scripts;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.NCINO.scripts.testscripts.Reusable;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


@Test
public class ConsumerCustomer {
	
	
	public void login() throws Exception{
	
	//Reusable pe= new Reusable();

		Reusable re= new Reusable();
		re.createChromeDriver();
		
		WebDriver driver = new ChromeDriver();			
		driver.get("https://test.salesforce.com/");		
		driver.manage().window().maximize();	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);				
		WebDriverWait wait = new WebDriverWait(driver, (long) 25);				  	
		String encodedPassword=""; 
		
		
		re.setElement(driver, "xpath","//*[@id='username']","prosenjit.paul@flagstar.com.ncinodev");
	re.setElement(driver, "xpath", "//*[@id='password']", (decodeStr(encodedPassword)));	
	//re.setElement(driver, "xpath", "//*[@id='password']", "");
	re.Click(driver, "//*[@id='Login']");	
	Thread.sleep(5000);
	Random rnd = new Random();
	int n = 100000 + rnd.nextInt(900000);
	int d = 1000 + rnd.nextInt(4000);
	
	
	//try 
	//{
		
	JavascriptExecutor jse = (JavascriptExecutor)driver;
	Thread.sleep(3000);
	 driver.findElement(By.linkText("Customers")).click();
	    

driver.findElement(By.name("new")).click();

driver.findElement(By.id("p3")).click();
driver.findElement(By.xpath("(//input[@name='save'])[2]")).click();
driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).click();
driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).clear();
driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id81:j_id83")).sendKeys("Loamchenko");
driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id84:j_id86")).clear();
driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id84:j_id86")).sendKeys("Vasily");
driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id93:j_id95")).click();
driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id93:j_id95")).clear();
driver.findElement(By.name("j_id0:j_id60:j_id61:customerSearch:pgBlck:j_id80:j_id93:j_id95")).sendKeys("666"+n);
driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:Search")).click();
driver.findElement(By.id("j_id0:j_id60:j_id61:customerSearch:pgBlck:addNew")).click();
driver.findElement(By.id("pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2")).click();
//new Select(driver.findElement(By.id("pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2"))).selectByVisibleText("Other");
driver.findElement(By.id("pg:frm:pb:welcomeSection:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList2")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:PgSection121:section1:6:section2:section4")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:PgSection121:section1:6:section2:section4")).clear();
driver.findElement(By.id("pg:frm:pb:PgSection121:PgSection121:section1:6:section2:section4")).sendKeys("666"+n);
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:1:section7:section11")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:1:section7:section11")).clear();
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:1:section7:section11")).sendKeys("2125679870");
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:4:section7:section9")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:4:section7:section9")).clear();
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:4:section7:section9")).sendKeys("loma@test.com");
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:7:section7:section9")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:7:section7:section9")).clear();
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:7:section7:section9")).sendKeys("chenko");
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:10:section7:section9")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:10:section7:section9")).clear();
driver.findElement(By.id("pg:frm:pb:PgSection121:j_id347:section6:10:section7:section9")).sendKeys("11/11/1989");
driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id367")).click();
driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id367")).clear();
driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section15:j_id367")).sendKeys("45 nomas st");
driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section16:j_id374")).click();
driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section16:j_id374")).clear();
driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section16:j_id374")).sendKeys("troy");
driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section20:j_id427")).click();
driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section20:j_id427")).clear();
driver.findElement(By.id("pg:frm:pb:addresssection1:addresssection2:section20:j_id427")).sendKeys("48084");
driver.findElement(By.xpath("//div[@id='pg:frm:pb:addresssection1:addresssection7']/div/table/tbody/tr[9]/td")).click();
driver.findElement(By.name("pg:frm:pb:j_id636")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection:item3:iddt11")).click();


new Select(driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection:item3:iddt11"))).selectByVisibleText("Driver's License");
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection:item3:iddt11")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection:j_id106:iddn")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection:j_id106:iddn")).clear();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection:j_id106:iddn")).sendKeys("P56565");
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection:j_id174:expd")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection:j_id174:expd")).clear();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection:j_id174:expd")).sendKeys("12/13/2022");
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id206:type2")).click();
new Select(driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id206:type2"))).selectByVisibleText("Credit Card");
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id206:type2")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id215:ctype")).click();
new Select(driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id215:ctype"))).selectByVisibleText("Visa");
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id215:ctype")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id222:aiddn")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id222:aiddn")).clear();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id222:aiddn")).sendKeys("7865");
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id296:aexpd")).click();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id296:aexpd")).clear();
driver.findElement(By.id("pg:frm:pb:PgSection121:pgSection1:j_id296:aexpd")).sendKeys("09/24/2023");
driver.findElement(By.xpath("//div[@id='pg:frm:pb']/div")).click();
driver.findElement(By.id("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList1")).click();
new Select(driver.findElement(By.id("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList1"))).selectByVisibleText("Employed");
driver.findElement(By.id("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:theitem:selectList1")).click();
driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:0:j_id529")).click();
new Select(driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:0:j_id529"))).selectByVisibleText("Construction and Skilled Trades");
driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:0:j_id529")).click();
driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:2:j_id462")).click();
driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:2:j_id462")).clear();
driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:2:j_id462")).sendKeys("Boxer");
driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:3:j_id462")).click();
driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:3:j_id462")).clear();
driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:3:j_id462")).sendKeys("Top Rank");
driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:5:j_id533")).click();
new Select(driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:5:j_id533"))).selectByVisibleText("3+");
driver.findElement(By.name("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:5:j_id533")).click();
driver.findElement(By.id("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:0:j_id447:childpanel:6:j_id542:0")).click();
driver.findElement(By.id("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1")).click();
new Select(driver.findElement(By.id("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1"))).selectByVisibleText("Yes");
driver.findElement(By.id("pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:1:theitem:selectList1")).click();
// ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:2:theitem:multiSelect | label=None]]
driver.findElement(By.xpath("//option[@value='None']")).click();
// ERROR: Caught exception [ERROR: Unsupported command [addSelection | id=pg:frm:pb:j_id314:comp:pgblk:pgblk1:pgblktable1:3:theitem:multiSelect | label=Personal Savings]]
driver.findElement(By.xpath("(//option[@value='Personal Savings'])[2]")).click();
driver.findElement(By.id("pg:frm:pb:j_id1029")).click();
driver.findElement(By.xpath("(//input[@name='save'])[2]")).click();
driver.findElement(By.id("j_id0:Applications:mainBlock:j_id159:j_id164:checki")).click();
new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id159:j_id164:checki"))).selectByVisibleText("1");
driver.findElement(By.id("j_id0:Applications:mainBlock:j_id159:j_id164:checki")).click();
driver.findElement(By.id("j_id0:Applications:mainBlock:j_id187:j_id195:0:j_id197")).click();
new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id187:j_id195:0:j_id197"))).selectByVisibleText("Individual");
driver.findElement(By.id("j_id0:Applications:mainBlock:j_id187:j_id195:0:j_id197")).click();
driver.findElement(By.id("j_id0:Applications:mainBlock:j_id187:j_id202:0:j_id206")).click();
new Select(driver.findElement(By.id("j_id0:Applications:mainBlock:j_id187:j_id202:0:j_id206"))).selectByVisibleText("Not Applicable");
driver.findElement(By.id("j_id0:Applications:mainBlock:j_id187:j_id202:0:j_id206")).click();
driver.findElement(By.name("j_id0:Applications:mainBlock:j_id490:j_id492")).click();
// ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=j_id0:frm:equifaxTable:j_id167:j_id210:0:j_id225:0:j_id227 | label=Primary Owner]]
driver.findElement(By.xpath("//option[@value='Primary Owner']")).click();
driver.findElement(By.id("j_id0:frm:equifaxTable:ValidateSubmitEquifax")).click();
driver.findElement(By.id("j_id0:frm:equifaxTable:j_id449:j_id455:0:j_id457:j_id459:0")).click();
driver.findElement(By.id("j_id0:frm:equifaxTable:j_id449:j_id461:0:j_id463:j_id465:0")).click();
driver.findElement(By.id("j_id0:frm:equifaxTable:j_id449:j_id467:equiFaxButton")).click();
driver.findElement(By.id("j_id0:frm:equifaxTable:j_id320:decisionTable:0:warningInput")).click();
new Select(driver.findElement(By.id("j_id0:frm:equifaxTable:j_id320:decisionTable:0:warningInput"))).selectByVisibleText("Pass");
driver.findElement(By.id("j_id0:frm:equifaxTable:j_id320:decisionTable:0:warningInput")).click();
driver.findElement(By.id("j_id0:frm:equifaxTable:j_id320:decisionTable:0:warningOverrideInput")).click();
driver.findElement(By.id("j_id0:frm:equifaxTable:j_id320:decisionTable:0:warningOverrideInput")).clear();
driver.findElement(By.id("j_id0:frm:equifaxTable:j_id320:decisionTable:0:warningOverrideInput")).sendKeys("Passed and Won");
driver.findElement(By.name("j_id0:frm:equifaxTable:j_id496")).click();
driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:Product:j_id98")).click();
new Select(driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:Product:j_id98"))).selectByVisibleText("SimplyChecking");
driver.findElement(By.name("pg:frm:pb:b2cList:0:Checking:Product:j_id98")).click();
driver.findElement(By.id("pg:frm:pb:b2cList:0:Checking:j_id143:regOption")).click();
new Select(driver.findElement(By.id("pg:frm:pb:b2cList:0:Checking:j_id143:regOption"))).selectByVisibleText("Yes");
driver.findElement(By.id("pg:frm:pb:b2cList:0:Checking:j_id143:regOption")).click();
driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:0:j_id706:0")).click();
driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:2:j_id706:0")).click();
driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:6:j_id706:0")).click();
driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:8:j_id706:0")).click();
driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:12:j_id706:1")).click();
driver.findElement(By.id("pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:14:j_id706:0")).click();
driver.findElement(By.name("pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:16:j_id700")).click();
new Select(driver.findElement(By.name("pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:16:j_id700"))).selectByVisibleText("In person - In Branch/ Office");
driver.findElement(By.name("pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:16:j_id700")).click();
// ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:18:j_id711 | label=Business Proceeds]]
driver.findElement(By.xpath("//option[@value='Business Proceeds']")).click();
// ERROR: Caught exception [ERROR: Unsupported command [addSelection | name=pg:frm:pb:b2cList:0:kycSection2:j_id627:j_id628:j_id690:20:j_id711 | label=Salary]]
driver.findElement(By.xpath("(//option[@value='Salary'])[2]")).click();
driver.findElement(By.linkText("Next")).click();
driver.findElement(By.name("addser:frm:j_id985")).click();
driver.findElement(By.id("j_id0:frm:j_id83:j_id98:0:j_id100:fundingcategorysection:fundingCategoryText")).click();
new Select(driver.findElement(By.id("j_id0:frm:j_id83:j_id98:0:j_id100:fundingcategorysection:fundingCategoryText"))).selectByVisibleText("Cash & Check");
driver.findElement(By.id("j_id0:frm:j_id83:j_id98:0:j_id100:fundingcategorysection:fundingCategoryText")).click();
driver.findElement(By.id("j_id0:frm:j_id83:j_id98:0:j_id100:j_id134:j_id136")).click();
driver.findElement(By.id("j_id0:frm:j_id83:j_id98:0:j_id100:j_id134:j_id136")).clear();
driver.findElement(By.id("j_id0:frm:j_id83:j_id98:0:j_id100:j_id134:j_id136")).sendKeys("4500");
driver.findElement(By.name("j_id0:frm:j_id83:j_id147:j_id151")).click();


driver.switchTo().alert().accept();
driver.findElement(By.id("j_id0:frm:j_id195:j_id253:setupUpAccountsbutton")).click();

driver.findElement(By.id("j_id0:frm:pgblck:j_id102:0:pgblcksection:j_id105:0:j_id106:0:pbs1:j_id107:0:j_id125:2")).click();
driver.findElement(By.id("j_id0:frm:pgblck:j_id102:0:pgblcksection:j_id105:1:j_id106:0:pbs1:j_id107:0:j_id125:2")).click();
driver.findElement(By.id("j_id0:frm:pgblck:j_id102:0:pgblcksection:j_id105:1:j_id106:0:pbs1:j_id107:1:j_id125:2")).click();
driver.findElement(By.id("j_id0:frm:pgblck:a:fin")).click();

	

	
	

	}







	
	public static String decodeStr(String encodedStr) {
	    byte[] decoded = Base64.decodeBase64(encodedStr);
	                    
	    return new String(decoded);
	
		}
	
}